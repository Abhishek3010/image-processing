clc;
clear all;
close all;
i = imread('smear3.jpg');
if size(i,3)==3 %RGB image
    i=rgb2gray(i);
end
i1 = i;
imshow(i1);

i2 = edge(i1,'canny',0.9);
imshow(i2);

se = strel('square',20);
i3 = imdilate(i2,se);
imshow(i3);

i4 = imfill(i3,'holes');
imshow(i4);

[Ilabel, num] = bwlabel(i4);
disp(num);
Iprops = regionprops(Ilabel);
Ibox = [Iprops.BoundingBox];
Ibox = reshape(Ibox,4,[]);
imshow(i);

hold on;
for cnt = 1:size(Ibox,8)
   rectangle('position',Ibox(:,cnt),'edgecolor','r');
end