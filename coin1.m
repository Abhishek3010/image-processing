clc;
clear all;
close all;
a = imread('coins.jpg');
if size(a,3)==3 %RGB image
    a=rgb2gray(a);
end
BW = a > 90;
imshow(BW)
title('Image with Circles')
stats = regionprops('table',BW,'Centroid','MajorAxisLength','MinorAxisLength')
centers = stats.Centroid;
diameters = mean([stats.MajorAxisLength stats.MinorAxisLength],2);
radii = diameters/2;
hold on
viscircles(centers,radii,'color','g');
hold off
