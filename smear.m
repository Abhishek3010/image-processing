clc;
clear all;
close all;
rgb = imread('smear1.jpg');
figure;
imshow(rgb);
gray_image = rgb2gray(rgb);
%imshow(gray_image);
[size1,size2]=size(gray_image);
for i=1:size1
   for j=1:size2
      if gray_image(i,j)<100
          gray_image(i,j)=255;
      end
   end
end


im1=im2bw(gray_image,graythresh(gray_image));
figure;
imshow(im1);
%figure;
%imshow(im1);
[centers, radii, metric] = imfindcircles(rgb,[20 60],'ObjectPolarity','dark','Sensitivity',0.88,'Method','twostage');
h = viscircles(centers,radii);
[m,n]=size(centers);
disp(m); %RBC COUNT 
