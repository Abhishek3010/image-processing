clc;
clear all;
close all;
rgb = imread('coins.jpg');
imshow(rgb);
d = imdistline;
delete(d);
gray_image = rgb2gray(rgb);
%imshow(gray_image);
[centers,radii] = imfindcircles(gray_image,[88 92],'ObjectPolarity','dark', 'Sensitivity',0.99,'Method','twostage');
h = viscircles(centers,radii,'color','g');
title('circulated image');


